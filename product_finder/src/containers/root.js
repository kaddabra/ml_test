import React, { Fragment, useEffect } from "react";
import conatinerStyle from "../utils/conatinerStyle";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { itemList } from "../storage/selectors";

import SearchBar from "../utils/SearchBar/SearchBar";
import useSarchBar from "../utils/hooks/useSarchBar";

import Breadcrumb from "../utils/BreadCrumb/BreadCrumb";

import ListItems from "../views/home/ListItems";
import RouteMercadoLibre from "../views/router/Router";
import DetailItem from "../views/home/DetailItem";

export const Root = () => {
  const dispatch = useDispatch();
  const classes = conatinerStyle();

  const items = useSelector((state) => itemList(state));
  const { searchBar, filter, setFilter } = useSarchBar(dispatch);

  return (
    <Fragment>
      <BrowserRouter>
        <div className={classes.container}>
          <SearchBar handleSearch={searchBar} />
          <Breadcrumb categories={!!items && items.data.categories} />
          <Switch>
            <Route
              path="/"
              exact={true}
              render={(routeProps) => (
                <RouteMercadoLibre {...routeProps} filter={filter} />
              )}
            />
            <Route
              path="/items"
              exact={true}
              render={(routeProps) => (
                <ListItems {...routeProps} filter={!!filter && filter} />
              )}
            />
            <Route
              path="/items/:id"
              exact={true}
              render={(routeProps) => (
                <DetailItem {...routeProps} filter={!!filter && filter} />
              )}
            />
          </Switch>
        </div>
      </BrowserRouter>
    </Fragment>
  );
};
