import { BOOLEAN_FILTER_STATE } from "../constans/filterState";

export function getFilter(data) {
  return { type: BOOLEAN_FILTER_STATE, data };
}
