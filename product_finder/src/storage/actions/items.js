import { callService } from "../../service/services";
import { GET_ITEMS, GET_ITEM, RECHARGE_FILTER } from "../constans/items";

export function getItems(data, author, reduxName) {
  return { type: GET_ITEMS, data, author, reduxName };
}

export function getItem(data, author, reduxName) {
  return { type: GET_ITEM, data, author, reduxName };
}

export function getFilter(data, reduxName) {
  return { type: RECHARGE_FILTER, data, reduxName };
}

export const getTotalItems = (route, params, reduxName) => async (dispatch) => {
  let response = await callService(route, params);
  dispatch(getItems(response, response.data.author, reduxName));

  return response;
};

export const getItemId = (route, params, reduxName) => async (dispatch) => {
  let response = await callService(route, params);
  dispatch(getItem(response, response.data.author, reduxName));

  return response;
};
