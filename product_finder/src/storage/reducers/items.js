import initialState from "./initialState";
import { GET_ITEMS, GET_ITEM, RECHARGE_FILTER } from "../constans/items";

export default function reducer(state = initialState.items, action) {
  switch (action.type) {
    case GET_ITEMS:
      return {
        ...state,
        [action.reduxName]: { data: action.data.data, author: action.author },
      };

    case GET_ITEM:
      return {
        ...state,
        [action.reduxName]: {
          data: action.data.data.item,
          author: action.author,
        },
      };

    case RECHARGE_FILTER:
      return {
        ...state,
        [action.reduxName]: { filter: action.data },
      };

    default:
      return state;
  }
}
