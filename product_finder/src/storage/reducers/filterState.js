import initialState from "./initialState";
import { BOOLEAN_FILTER_STATE } from "../constans/filterState";

export default function reducer(state = initialState.filterState, action) {
  switch (action.type) {
    case BOOLEAN_FILTER_STATE:
      return action.data;

    default:
      return state;
  }
}
