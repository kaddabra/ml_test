import { combineReducers } from "redux";
import items from "./items";
import filterState from "./filterState";

export default combineReducers({
  items,
  filterState,
});
