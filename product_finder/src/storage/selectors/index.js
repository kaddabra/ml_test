import { get } from "lodash";

export const itemList = (state) => get(state, "items.totalItems");
export const filterRecharge = (state) => get(state, "items.filter");
export const itemDetail = (state) => get(state, "items.itemDetail");
export const filterState = (state) => get(state, "filterState");
