export const formatNumber = (num, decimal = false) => {
  let numClear = typeof num == "string" ? num.replace(/,/g, "") : num;
  numClear = Intl.NumberFormat("en-US", {
    maximuntFractionDigits: decimal ? 4 : 2,
    maximumIntegerDigits: 8,
  }).format(parseFloat(numClear));
  return numClear;
};
