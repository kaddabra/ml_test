import React, { useState, useEffect } from "react";
import stylesSearchBar from "./stylesSearchBar";
import { Grid, InputBase, IconButton } from "@material-ui/core";
import { Formik, Field, Form } from "formik";
import useLocalHandleChange from "../hooks/useLocalHandleChange";
import CalculatedIcon from "../CalculatedIcon";
import { SEARCH_ICON, LOGO_ML } from "../assets";

import { useSelector } from "react-redux";
import { filterRecharge } from "../../storage/selectors";

const MaterialUiInput = ({ onChange, field, form, ...props }) => {
  const classes = stylesSearchBar();
  let { handleChange } = form;
  const { localHandleChange } = useLocalHandleChange(handleChange, onChange);

  return (
    <InputBase
      id="searchBar"
      {...field}
      {...props}
      className={classes.input}
      onChange={localHandleChange}
      placeholder="Nunca dejes de buscar"
      inputProps={{ "aria-label": "Buscar" }}
    />
  );
};

const SearchBar = (props) => {
  const classes = stylesSearchBar();
  const filterText = useSelector((state) => filterRecharge(state));
  const [filter, setFilter] = useState("");

  useEffect(() => {
    setFilter(!!filterText && !!filterText.filter ? filterText.filter : "");
  }, [filterText]);

  return (
    <Grid className={classes.container}>
      <CalculatedIcon {...LOGO_ML} className={classes.marginRight} />
      <Formik
        initialValues={{ searchBar: "" }}
        enableReinitialize={true}
        onSubmit={!!props.handleSearch && props.handleSearch}
      >
        {() => (
          <Form className={classes.paperBar}>
            <Field
              component={MaterialUiInput}
              name="searchBar"
              onChange={!!props.onChange && props.onChange}
            />
            <div className={classes.iconButton}>
              <IconButton type="submit" aria-label="buscar">
                <CalculatedIcon {...SEARCH_ICON} />
              </IconButton>
            </div>
          </Form>
        )}
      </Formik>
    </Grid>
  );
};

export default SearchBar;
