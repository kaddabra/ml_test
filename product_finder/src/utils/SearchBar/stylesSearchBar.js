import { makeStyles } from "@material-ui/core";

const stylesSearchBar = makeStyles((theme) => ({
  container: {
    backgroundColor: " #FFE600",
    height: 60,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  paperBar: {
    height: 36,
    width: "70%",
    padding: "2px 0px",
    display: "flex",
    alignItems: "center",
    boxSizing: "border-box",
    backgroundColor: "#FFFFFF",
    boxShadow:
      "0 1px 1px 0 rgba(0,0,0,0.14), 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 3px 0 rgba(0,0,0,0.2)",
  },
  iconButton: {
    height: 36,
    backgroundColor: " #EEEEEE",
    display: "flex",
    alignContent: "center",
  },
  marginRight: {
    marginRight: 35,
  },
}));

export default stylesSearchBar;
