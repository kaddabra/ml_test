import searchIcon from "../assets/images/ic_Search@2x.png";
import logoMl from "../assets/images/Logo_ML@2x.png";
import shippingLogo from "../assets/images/ic_shipping@2x.png";

export const SEARCH_ICON = {
  src: searchIcon,
  style: { width: 20, height: 20 },
  alt: "search_icon",
};

export const LOGO_ML = {
  src: logoMl,
  style: { width: 60, height: 40 },
  alt: "logo_mercado_libre",
};

export const SHIPPING_LOGO = {
  src: shippingLogo,
  style: { width: 15, height: 15, marginLeft: 16 },
  alt: "logo_mercado_libre",
};
