const useLocalHandleChange = (handleChange, onChange) => {
  const localHandleChange = (event) => {
    handleChange(event);
    !!onChange && onChange(event.target.value);
  };
  return {
    localHandleChange,
  };
};

export default useLocalHandleChange;
