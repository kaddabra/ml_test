import { useState } from "react";
import { getTotalItems } from "../../storage/actions/items";
import { getFilter } from "../../storage/actions/filterState";

const useSarchBar = (dispatch) => {
  const [filter, setFilter] = useState(undefined);
  const searchBar = (filterText) => {
    setFilter(filterText.searchBar);
    dispatch(
      getTotalItems(
        "api/items",
        { q: filterText.searchBar },
        "totalItems"
      )
    );
    dispatch(getFilter(true));
  };
  return {
    searchBar,
    filter,
    setFilter,
  };
};

export default useSarchBar;
