import { makeStyles } from "@material-ui/core";

const styleProductCard = makeStyles((theme) => ({
  card: {
    backgroundColor: "#FFFFFF",
    height: "212px",
    width: "75%",
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    borderBottom: "1px solid #EEEEEE",
  },
  imageCard: {
    width: " 180px",
    height: "180px",
    borderRadius: "4px",
    marginLeft: 16,
    cursor: "pointer",
  },
  contentCard: {
    marginLeft: 16,
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
  },
  PriceBox: {
    marginBottom: 32,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  fontPrice: {
    fontSize: 22,
    fontWeight: 400,
    color: "#333333",
  },
  fontDescription: {
    fontSize: 20,
    fontWeight: 300,
    color: "#666666",
    lineHeight: 1.3,
  },
}));

export default styleProductCard;
