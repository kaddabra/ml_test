import React from "react";
import { useDispatch } from "react-redux";
import { getFilter } from "../../storage/actions/filterState";
import { Grid, Typography } from "@material-ui/core";
import styleProductCard from "./styleProductCard";
import { formatNumber } from "../formatNumbre";
import { SHIPPING_LOGO } from "../assets";
import CalculatedIcon from "../CalculatedIcon";
import { Mock } from "./mockCard";

const detailProduct = (id, history, filter, dispatch) => {
  dispatch(getFilter(false));
  history.push({
    pathname: `/items/${id}`,
    state: { fromDashboard: true, filter },
  });
};

const ProductCard = (props) => {
  const classes = styleProductCard();
  const dispatch = useDispatch();
  const { data, history, filter } = props;
  // const infoCard = Mock;
  const infoCard = data;

  return (
    <Grid className={classes.card}>
      <img
        src={infoCard.picture}
        className={classes.imageCard}
        onClick={() => detailProduct(infoCard.id, history, filter, dispatch)}
      />
      <Grid
        className={classes.contentCard}
        onClick={() => detailProduct(infoCard.id, history, filter, dispatch)}
      >
        <Grid className={classes.PriceBox}>
          <Typography className={classes.fontPrice}>
            {"$ " + formatNumber(infoCard.price.amount)}
          </Typography>
          {infoCard.free_shipping && <CalculatedIcon {...SHIPPING_LOGO} />}
        </Grid>
        <Typography className={classes.fontDescription}>
          {infoCard.tittle + "  "}{" "}
          {infoCard.condition == "new" ? "nuevo" : "usado"}
        </Typography>
        <Typography className={classes.fontDescription}>
          {"Completo Unico!"}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default ProductCard;
