import React, { Fragment } from "react";
import stylesBreadCrumb from "./stylesBreadCrumb";
import { Breadcrumbs, Link, Grid } from "@material-ui/core";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";

const Breadcrumb = (props) => {
  const classes = stylesBreadCrumb();
  const { categories } = props;

  return (
    <Grid className={classes.content}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
        {!!categories &&
          categories.map((type, breadIndex) => (
            <Link color="inherit" key={breadIndex}>
              {type}
            </Link>
          ))}
        {/* <Link color="inherit" href="/">
          Mercado Libre
        </Link> */}
      </Breadcrumbs>
    </Grid>
  );
};

export default Breadcrumb;
