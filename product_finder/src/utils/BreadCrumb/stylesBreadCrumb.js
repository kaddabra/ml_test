import { makeStyles } from "@material-ui/core";

const stylesBreadCrumb = makeStyles((theme) => ({
  content: {
    marginLeft: "12.5%",
    marginTop: 16,
    marginBottom: 16,
  },
}));

export default stylesBreadCrumb;
