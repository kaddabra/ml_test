import axios from "axios";
import { CONTS } from "./CONTS";

export const callService = async (serviceName, params) => {
  try {
    const response = await axios.get(CONTS.urlService + serviceName, {
      "Content-Type": "application/json",
      params,
    });
    return response;
  } catch (error) {
    throw error;
  }
};
