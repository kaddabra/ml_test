import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Root } from "../src/containers/root";
import * as serviceWorker from "./serviceWorker";
import conatinerStyle from "./utils/conatinerStyle";

import { Provider } from "react-redux";
import store from "./storage/store";

ReactDOM.render(
  <Provider store={store}>
    <React.Fragment>
      <Root />
    </React.Fragment>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
