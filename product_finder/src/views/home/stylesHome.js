import { makeStyles } from "@material-ui/core";

const stylesHome = makeStyles((theme) => ({
  containerList: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 32,
  },
  cards: {
    marginTop: 16,
    marginBottom: 16,
    borderBottom: "1px solid red",
  },
  cardItem: {
    backgroundColor: "#FFFFFF",
    width: "1104px",
    marginBottom: 32,
  },
  imageCard: {
    width: " 680px",
    height: "680px",
    cursor: "pointer",
  },
  saleProductBox: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 40,
  },
  stateFont: {
    fontSize: 14,
    color: "#666666",
    marginBottom: 16,
    marginTop: 32,
  },
  tittleFont: {
    fontSize: 26,
    fontWeight: "600",
    textOverflow: "ellipsis",
    lineHeight: 1.05,
    color: "#333333",
  },
  tittleBox: {
    width: 360,
  },
  fontPrice: {
    fontSize: 44,
    fontWeight: "300",
    marginTop: 32,
  },
  fontDecimal: {
    fontSize: 22,
    fontWeight: "300",
    marginTop: 38,
    marginLeft: 5,
  },
  constBox: {
    display: "flex",
    flexDirection: "row",
  },
  fontButton: {
    fontSize: 15,
    color: "#FFFFFF",
  },
  buttonBox: {
    marginTop: 32,
    width: 320,
    height: 46,
    backgroundColor: "#3483FA",
  },
  descriptionBox: {
    marginLeft: 32,
    marginTop: 32,
    marginBottom: 32,
    width: 680,
  },
  descriptionFontTittle: {
    fontSize: 24,
    fontWeight: "600",
    marginBottom: 32,
    color: "#333333",
  },
  marginLeft: {
    marginLeft: 32,
  },
}));

export default stylesHome;
