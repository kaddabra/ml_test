import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getTotalItems, getFilter } from "../../storage/actions/items";
import { itemList } from "../../storage/selectors";
import stylesHome from "./stylesHome";

import ProductCard from "../../utils/ProductCard/ProductCard";
import { Grid } from "@material-ui/core";

const ListItems = (props) => {
  const classes = stylesHome();
  const dispatch = useDispatch();
  const { filter, history, location } = props;
  const items = useSelector((state) => itemList(state));
  const productList = !!items && !!items.data && items.data.items;

  useEffect(() => {
    if (filter) {
      history.push({
        pathname: "/items",
        search: `?search=${filter}`,
        state: { fromDashboard: true, filter },
      });
    } else {
      if (location.search) {
        const filter = location.search
          .replace("?search=", "")
          .replace(/%20/g, " ");
        document.getElementById("searchBar").placeholder = filter
          ? filter
          : "Nunca dejes de buscar";
        dispatch(getFilter(filter, "filter"));
        dispatch(getTotalItems("api/items", { q: filter }, "totalItems"));
      }
    }
  }, [filter]);

  return (
    <Grid className={classes.containerList}>
      {!!productList &&
        productList.map((product, indexProduct) => (
          <ProductCard
            data={product}
            key={indexProduct}
            className={classes.cards}
            filter={filter}
            {...props}
          />
        ))}
    </Grid>
  );
};

export default ListItems;
