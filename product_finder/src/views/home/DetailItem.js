import React, { useEffect, Fragment } from "react";
import stylesHome from "./stylesHome";
import { useSelector, useDispatch } from "react-redux";
import { getItemId, getTotalItems } from "../../storage/actions/items";
import { itemDetail, filterState } from "../../storage/selectors";
import { formatNumber } from "../../utils/formatNumbre";
import { Grid, Typography, Button } from "@material-ui/core";

const DetailItem = (props) => {
  const { location, filter, history } = props;
  const classes = stylesHome();
  const dispatch = useDispatch();
  const data = useSelector((state) => itemDetail(state));
  const filterBool = useSelector((state) => filterState(state));
  const itemData = !!data && data.data;

  useEffect(() => {
    const filter = location.pathname.replace("/items/", "");
    dispatch(getItemId(`api/items/${filter}`, {}, "itemDetail"));
    if (!!location.state && !!location.state.filter) {
      dispatch(
        getTotalItems(
          "api/items",
          { q: location.state.filter },
          "totalItems"
        )
      );
    }
  }, []);

  useEffect(() => {
    if (filterBool && !!filter) {
      history.push({
        pathname: "/items",
        search: `?search=${filter}`,
        state: { fromDashboard: true, filter },
      });
    }
  }, [filter, filterBool]);

  return (
    <Fragment>
      {!!itemData && (
        <Grid className={classes.containerList}>
          <Grid container className={classes.cardItem}>
            <Grid className={classes.saleProductBox}>
              <img src={itemData.picture} className={classes.imageCard} />
              <Grid className={classes.marginLeft}>
                <Typography className={classes.stateFont}>
                  {itemData.condition == "new" ? "Nuevo" : "Usado"}
                  {" - " + itemData.sold_quantity + " vendidos"}
                </Typography>
                <Grid className={classes.tittleBox}>
                  <Typography className={classes.tittleFont}>
                    {itemData.tittle}
                  </Typography>
                </Grid>
                <Grid className={classes.constBox}>
                  <Typography className={classes.fontPrice}>
                    {"$ " + formatNumber(itemData.price.amount)}
                  </Typography>
                  <Typography className={classes.fontDecimal}>
                    {itemData.price.decimals.toFixed(2).replace("0.", "")}
                  </Typography>
                </Grid>
                <Button className={classes.buttonBox}>
                  <Typography className={classes.fontButton}>
                    {"Comprar"}
                  </Typography>
                </Button>
              </Grid>
            </Grid>
            <Grid className={classes.descriptionBox}>
              <Typography className={classes.descriptionFontTittle}>
                {"Descripción del producto"}
              </Typography>
              <Typography className={classes.stateFont}>
                {itemData.description}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      )}
    </Fragment>
  );
};

export default DetailItem;
