import React, { Fragment, useEffect, useState } from "react";
import { Redirect } from "react-router-dom";

const RouteMercadoLibre = (props) => {
  const { filter } = props;

  return (
    <Fragment>
      No Search
      {filter && (
        <Redirect
          to={{
            pathname: "/items",
            search: `${filter}`,
            state: { fromDashboard: true },
          }}
        />
      )}
    </Fragment>
  );
};

export default RouteMercadoLibre;
