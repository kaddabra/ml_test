var urlAPI = "https://api.mercadolibre.com";
var author = { name: "Juan Pablo", lastname: "Esteban S" };
var express = require("express");
var fetch = require("node-fetch");
var router = express.Router();

function adaptItems(items) {
  const itemsSend = new Array();
  const newItems = items.slice(0, 4);
  for (let i = 0; i < newItems.length; i++) {
    itemsSend.push({
      id: newItems[i].id,
      tittle: newItems[i].title,
      price: {
        currency: newItems[i].currency_id,
        amount: Math.floor(newItems[i].price),
        decimals: newItems[i].price - Math.floor(newItems[i].price),
      },
      picture: newItems[i].thumbnail,
      condition: newItems[i].condition,
      free_shipping: newItems[i].shipping.free_shipping,
    });
  }
  return itemsSend;
}

function adaptCategories(categories) {
  let categoriesSend = new Array();
  for (let i = 0; i < categories.length; i++) {
    let categoriesElement = categories[i].values;
    for (let e = 0; e < categoriesElement.length; e++) {
      if (!!categoriesElement[e].path_from_root) {
        let internalCategories = categoriesElement[e].path_from_root;
        for (let a = 0; a < internalCategories.length; a++) {
          categoriesSend.push(internalCategories[a].name);
        }
      } else {
        categoriesSend.push(categoriesElement[e].name);
      }
    }
  }

  return categoriesSend;
}

router.get("/items", function (req, res, next) {
  fetch(urlAPI + "/sites/MLA/search?q=" + req.query.q)
    .then((res) => res.json())
    .then((data) => {
      res.send({
        author,
        items: adaptItems(data.results),
        categories:
          !!data.filters && data.filters.length > 0
            ? adaptCategories(data.filters)
            : [],
      });
    });
});

router.get("/items/:id", function (req, res, next) {
  let item = {};
  fetch(urlAPI + "/items/" + req.params.id.replace(":", ""))
    .then((res) => res.json())
    .then((data) => {
      item = {
        id: data.id,
        tittle: data.title,
        price: {
          amount: Math.floor(data.price),
          decimals: data.price - Math.floor(data.price),
          currency: data.currency_id,
        },
        picture: data.pictures[0].url,
        condition: data.condition,
        free_shipping: data.shipping.free_shipping,
        sold_quantity: data.sold_quantity,
      };
      fetch(
        urlAPI + "/items/" + req.params.id.replace(":", "") + "/description"
      )
        .then((res) => res.json())
        .then((description) => {
          item.description = description.plain_text;
          res.send({ author, item });
        });
    });
});

module.exports = router;
